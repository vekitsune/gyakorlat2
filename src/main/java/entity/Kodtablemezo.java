package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "kodtabla_mezo")
@Entity

public class Kodtablemezo implements Serializable {
    
    List<KodtablaErtekEntity>ertekek;

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "km_id ")
    private Integer kmid;
    
    @Column(name="mezonev")
    private String mezonev;
    
    @Column(name="adattipus")
    private String adattipus;
    
    @Column(name="leiras")
    private String leiras;

}
